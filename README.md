# Some useful commands

docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install

composer create-project symfony/website-skeleton .